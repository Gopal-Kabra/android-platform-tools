NAME = aidl-cpp
SOURCES = main_cpp.cpp
SOURCES := $(foreach source, $(SOURCES), system/tools/aidl/$(source))
CPPFLAGS += -I/usr/include/android \
	    -Isystem/tools/aidl 
CXXFLAGS += -std=c++17
LDFLAGS += -Wl,-rpath=/usr/lib/$(DEB_HOST_MULTIARCH)/android \
           -laidl-common \
           -Ldebian/out/system/tools/aidl -L/usr/lib/$(DEB_HOST_MULTIARCH)/android -lbase -latomic

$(NAME): $(SOURCES)
	mkdir --parents debian/out/system/tools/aidl
	$(CXX) $^ -o debian/out/system/tools/aidl/$(NAME) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS)
